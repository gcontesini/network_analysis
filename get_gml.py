# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import matplotlib.pyplot as plt
import networkx as nx
import itertools as it
import pygraphviz 
from networkx.algorithms import community
from networkx.drawing.nx_agraph import graphviz_layout

mygraph = nx.Graph()
mygraph = nx.read_graphml("./graphml/01.06.2018.graphml")
# mygraph = nx.drawing.nx_pydot.read_dot("./zipados/01.06.2018.NZ.dot")

degree = nx.average_degree_connectivity(mygraph)
p_graph = path_graph
clustering = it.girvan_newman(mygraph)
# avg_clustering = nx.average_clustering(mygraph)
# center = nx.center(mygraph)
# diameter = nx.diameter(mygraph)
avg_shortest_path = nx.average_shortest_path_length(mygraph)
# max_independent_set = nx.maximal_independent_set(mygraph)
# found_communities = community.girvan_newman(mygraph)
# print("Centralidade:",center)
print(degree)
print(clustering)
print(avg_shortest_path)

print(nx.is_tree(mygraph))

plt.figure(figsize=(16, 16))
pos = graphviz_layout(mygraph, prog='twopi', args='')
limits = plt.axis('off')
nx.draw_networkx(mygraph, pos, node_size=8, alpha=0.5, node_color="blue", with_labels=True, fontsize=2 )


# nx.draw(mygraph, with_labels=True)

# plt.show()
plt.savefig("twopi.pdf")