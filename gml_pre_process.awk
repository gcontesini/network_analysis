
#!/bin/awk -f

awk -F '"'  '
	BEGIN {
		print "Graph\t[\n\tdirected 1\n\tid ",ARGV[1],"\n\tlabel ",ARGV[1]
	}
	{	if(NF>1){
			print "\n\tnode\t[\n\t\t\tid",
			$2,
			"\n\t\t\tip",
			$2,
			"\n\t]"
		}
	}
	{
		if (NF>1){
			print "\n\tedge\t[\n\t\tsource\t",
			$2,
			"\n\t\ttarget\t",
			$4,
			"\n\t\tlabel\t\t",
			$6,
			"\n\t]"
		}
	}
	{

	}
	END	{
		print "]"
	}
	' ./zipados/01.06.2018.NZ.dot | awk NF > ./gml/01.06.2018.gml
