awk -F '"'  '
	BEGIN {
		print "<?xml version='1.0' encoding='utf-8'?>
					<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\"
					xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
					xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\">
					<graph edgedefault=\"directed\">"
	}
	{	
		if(NF>1){
			print "\n\t\t<node id=\"",
			$2,
			"\"/>\n"
		}
	}
	{
		if (NF>1){
			print "\t\t<edge id=\"",
			$6,
			"\" source=\"",
			$2,
			"\" target=\"",
			$4,
			"\"/>\n"
		}
	}
	{

	}
	END	{
		print "\t</graph>\n</graphml>"
	}
' ./zipados/01.06.2018.NZ.dot | awk NF | awk '!x[$0]++' > ./graphml/01.06.2018.graphml
